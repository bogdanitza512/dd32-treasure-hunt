// import data from "../../public/store.json";
import React, { Dispatch, createContext, useReducer } from "react";
import { Action, reducer } from "../reducers/store";
import * as THREE from "three";

export const initial = {
  current: 0,
  domes: [
    {
      name: "outside",
      color: "lightpink",
      position: new THREE.Vector3(10, 0, -15),
      url: "/360/la-plaza-del-callao-desde-la-calle-gran-via-madrid-espana.jpg",
      link: 1,
    },
    {
      name: "inside",
      color: "lightblue",
      position: new THREE.Vector3(15, 0, 0),
      url: "/360/madrid-from-circulo-bellas-artes.jpg",
      link: 0,
    },
  ],
  textures: [] as THREE.Texture[],
};

export type Store = typeof initial;
export type Content = [Store, Dispatch<Action>];

export const Context = createContext({} as Content);

export const Provider: React.FC = ({ children }) => {
  const payload = useReducer(reducer, initial);
  return <Context.Provider value={payload}>{children}</Context.Provider>;
};
