import React, { Suspense, useContext } from "react";
import { Canvas } from "react-three-fiber";
import { Html, OrbitControls } from "drei";
import "./App.css";
import Preload from "./components/Preload";
import { Context } from "./context/store";

const App = () => {
  const [store, dispatch] = useContext(Context);
  return (
    <Canvas invalidateFrameloop concurrent camera={{ position: [0, 0, 0.1] }}>
      <OrbitControls
        enableZoom={false}
        enablePan={false}
        enableDamping={true}
        dampingFactor={0.2}
        autoRotate={false}
        rotateSpeed={-0.5}
      />
      <Suspense
        fallback={
          <Html center style={{ color: "white" }}>
            loading...
          </Html>
        }
      >
        <h1>{store.current}</h1>
        {/* <Preload /> */}
        {/* <Portals store={store} /> */}
      </Suspense>
    </Canvas>
  );
};

export default App;
