import { Store } from "../context/store";
import { ActionType as ActionOfType } from "../common/types";

export enum Type {
  SET_NOTE_ID = "SET_NOTE_ID",
  SET_PERFORMED_SCAN = "SET_PERFORMED_SCAN",
  SET_CURRENT = "SET_CURRENT",
  ADD_TEXTURE = "ADD_TEXTURE",
}

export const actions = {
  setCurrent: (value: number) => {
    return {
      type: Type.SET_CURRENT,
      value,
    } as const;
  },
  addTexture: (texture: THREE.Texture) => {
    return {
      type: Type.ADD_TEXTURE,
      texture,
    } as const;
  },
};

export type Action = ActionOfType<typeof actions>;

export const reducer = (state: Store, action: Action): Store => {
  switch (action.type) {
    case Type.SET_CURRENT:
      return { ...state, current: action.value };
    case Type.ADD_TEXTURE:
      return { ...state, textures: [...state.textures, action.texture] };
    default:
      return state;
  }
};
