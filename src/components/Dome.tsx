import * as THREE from "three";
import { Html, OrbitControls } from "drei";
import { Canvas, useThree, useLoader } from "react-three-fiber";
import React, { Suspense, useRef, useState, useEffect } from "react";

interface Props {
  name: string;
  position: THREE.Vector3;
  color: string;
  texture: THREE.Texture;
  onClick: () => void;
}

const Dome = ({ name, position, color, texture, onClick }: Props) => {
  const ref = useRef();
  const [hovered, set] = useState(false);
  useEffect(
    () => void (document.body.style.cursor = hovered ? "pointer" : "auto"),
    [hovered]
  );
  return (
    <group>
      <mesh>
        <sphereBufferGeometry attach="geometry" args={[500, 60, 40]} />
        <meshBasicMaterial
          attach="material"
          map={texture}
          side={THREE.BackSide}
        />
      </mesh>
      <mesh
        ref={ref}
        position={position}
        onPointerOver={() => set(true)}
        onPointerOut={() => set(false)}
      >
        <sphereGeometry attach="geometry" args={[1, 32, 32]} />
        <meshBasicMaterial
          attach="material"
          color={hovered ? "black" : color}
        />
        <Html center>
          {/* <Popconfirm
            title="Are you sure you want to leave?"
            onConfirm={onClick}
            okText="Yes"
            cancelText="No"
          >
            <a href="#">{name}</a>
          </Popconfirm> */}
        </Html>
      </mesh>
    </group>
  );
};

export default Dome;
