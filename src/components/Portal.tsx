import React, { useContext } from "react";
import { Context } from "../context/store";
import { actions } from "../reducers/store";
import * as THREE from "three";
import { useTextureLoader } from "drei";
import Dome from "./Dome";

interface Props {
  scene: {
    name: string;
    color: string;
    position: THREE.Vector3;
    url: string;
    link: number;
  };
}

const Portals = (props: Props) => {
  const [{ current, domes }, dispatch] = useContext(Context);
  const { link, url, ...rest } = domes[current];
  const texture = useTextureLoader(url) as THREE.Texture;

  return (
    <Dome
      onClick={() => dispatch(actions.setCurrent(link))}
      texture={texture}
      {...rest}
    />
  );
};

export default Portals;
