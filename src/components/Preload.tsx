// import React, { useCallback, useContext, useEffect } from "react";
import { useTextureLoader } from "drei";
import React, { useEffect, useContext } from "react";
import { useThree } from "react-three-fiber";
import { Context } from "../context/store";
// import { actions } from "../reducers/store";

export const Preload = () => {
  const { gl } = useThree();
  const [store, dispatch] = useContext(Context);

  // useEffect(() => store.textures.forEach(gl.initTexture), [store.textures, gl]);
  // const onLoaded = useCallback(
  //   (texture: THREE.Texture) => dispatch(actions.addTexture(texture)),
  //   [dispatch]
  // );
  return (
    <>
      {store}
      {/* {store.domes.map(({ url }) => (
        <TextureLoader url={url} onLoaded={onLoaded} />
      ))} */}
    </>
  );
};

interface Props {
  url: string;
  onLoaded: (texture: THREE.Texture) => void;
}

export const TextureLoader = ({ url, onLoaded }: Props) => {
  const texture = useTextureLoader(url) as THREE.Texture;
  useEffect(() => onLoaded(texture), [onLoaded, texture]);
  return null;
};

export default Preload;
